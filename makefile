OBJS	= main.o ant.o
SOURCE	= main.cpp ant.cpp
HEADER	= ant.hpp
OUT	= langtonsAnt
CC	 = g++
FLAGS	 = -std=c++11 -g -c -Wall
LFLAGS	 = 

all: $(OBJS)
	$(CC) -g $(OBJS) -o $(OUT) $(LFLAGS)

main.o: main.cpp
	$(CC) $(FLAGS) main.cpp 

ant.o: ant.cpp
	$(CC) $(FLAGS) ant.cpp 


clean:
	rm -f $(OBJS) $(OUT)
