/*********************************************************************
 ** Program name: LangtonsAnt
 ** Author: David Anderson
 ** Date: 01/12/2019
 ** Description: This program runs the Langton's ant cellular automation
 *********************************************************************/

#ifndef ant_h
#define ant_h

enum directionFacing {NORTH = 1, EAST = 2, SOUTH = 3, WEST = 4};

class ant
{
private:
    directionFacing orientation = NORTH;    //defaults the ant to point up
    int boardRows,
        boardColumns,
        simulationSteps,
        antRow,
        antColumn;
    char colorToChange = ' '; //defaults to space/white because that is the first square
    char *board[];
    
public:
//class
    ant(int, int, int, int, int);   //the ant class takes in the board information and ant placement
    
    void runAnt();
    void setupBoard();
    void createBoard();

};
    
#endif /* ant_h */
