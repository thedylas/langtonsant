/*********************************************************************
 ** Program name: LangtonsAnt
 ** Author: David Anderson
 ** Date: 01/12/2019
 ** Description: This program runs the Langton's ant cellular automation
 *********************************************************************/

#include <iostream>
#include "ant.hpp"

using std::cout;
using std::cin;

ant::ant(int br, int bc, int ss, int ar, int ac) {
    boardRows = br;
    boardColumns = bc;
    simulationSteps = ss;
    antRow = ar - 1;    //these are -1 because arrays start at 0
    antColumn = ac - 1;
}

void ant::runAnt() {
    //dynamically allocates array to hold chars
    char **board = new char *[boardRows]; //creates rows
    for (int i = 0; i < boardColumns; i++) {
        board[i] = new char[boardColumns]; //creates columns
    }

    //fills array with spaces
    for (int row = 0; row < boardRows; row++) {
        for (int column = 0; column < boardColumns; column++) {
            board[row][column] = ' ';
        }
    }
    //displays board to user
    for (int printRow = 0; printRow < boardRows; printRow++) {
        int printColumn = 0;
        cout << board[printRow][printColumn];

        for (printColumn = 1; printColumn < boardColumns; printColumn++) {
            cout << board[printRow][printColumn];
        }
        cout << "\n";
    }

    //places the ant on the board
    cout << "Placing the ant now...\n";
    board[antRow][antColumn] = '*';

    //displays board to user
    for (int printRow = 0; printRow < boardRows; printRow++) {
        int printColumn = 0;
        cout << board[printRow][printColumn];

        for (printColumn = 1; printColumn < boardColumns; printColumn++) {
            cout << board[printRow][printColumn];
        }
        cout << "\n";
    }

    //begins the simulation    
    for (int i = 1; i <= simulationSteps; i++) {
        //signals separate output to the user
        std::cout << "-\n";
        switch (colorToChange) {
            //if the space that the ant is on is white
            case ' ': {
                //flips the board color
                board[antRow][antColumn] = '#';
                switch (orientation) {
                    case NORTH: {
                        //checks for the edge of the board, turns around if it is at the edge
                        if ((antColumn + 1) >= boardColumns) {
                            orientation = WEST;
                            antColumn--;
                        } else {
                            orientation = EAST;
                            antColumn++;
                        }
                        //stores the ants location to change it in the next step
                        colorToChange = board[antRow][antColumn];
                        //places the ant
                        board[antRow][antColumn] = '*';

                        //displays board to user
                        for (int printRow = 0; printRow < boardRows; printRow++) {
                            int printColumn = 0;
                            cout << board[printRow][printColumn];

                            for (printColumn = 1; printColumn < boardColumns; printColumn++) {
                                cout << board[printRow][printColumn];
                            }
                            cout << "\n";
                        }
                    }
                        break;

                    case EAST: {
                        //checks for the edge of the board, turns around if it is at the edge
                        if ((antRow + 1) >= boardRows) {
                            orientation = NORTH;
                            antRow--;
                        } else {
                            orientation = SOUTH;
                            antRow++;
                        }
                        //stores the ants location to change it in the next step
                        colorToChange = board[antRow][antColumn];
                        //places the ant
                        board[antRow][antColumn] = '*';

                        //displays board to user
                        for (int printRow = 0; printRow < boardRows; printRow++) {
                            int printColumn = 0;
                            cout << board[printRow][printColumn];

                            for (printColumn = 1; printColumn < boardColumns; printColumn++) {
                                cout << board[printRow][printColumn];
                            }
                            cout << "\n";
                        }
                    }
                        break;

                    case SOUTH: {
                        //checks for the edge of the board, turns around if it is at the edge
                        if ((antColumn - 1) < 0) {
                            orientation = EAST;
                            antColumn++;
                        } else {
                            orientation = WEST;
                            antColumn--;
                        }
                        //stores the ants location to change it in the next step
                        colorToChange = board[antRow][antColumn];
                        //places the ant
                        board[antRow][antColumn] = '*';

                        //displays board to user
                        for (int printRow = 0; printRow < boardRows; printRow++) {
                            int printColumn = 0;
                            cout << board[printRow][printColumn];

                            for (printColumn = 1; printColumn < boardColumns; printColumn++) {
                                cout << board[printRow][printColumn];
                            }
                            cout << "\n";
                        }
                    }
                        break;

                    case WEST: {
                        //checks for the edge of the board, turns around if it is at the edge
                        if ((antRow - 1) < 0) {
                            orientation = SOUTH;
                            antRow++;
                        } else {
                            orientation = NORTH;
                            antRow--;
                        }
                        //stores the ants location to change it in the next step
                        colorToChange = board[antRow][antColumn];
                        //places the ant
                        board[antRow][antColumn] = '*';

                        //displays board to user
                        for (int printRow = 0; printRow < boardRows; printRow++) {
                            int printColumn = 0;
                            cout << board[printRow][printColumn];

                            for (printColumn = 1; printColumn < boardColumns; printColumn++) {
                                cout << board[printRow][printColumn];
                            }
                            cout << "\n";
                        }
                    }
                        break;
                }
            }
                break;

                //if the space that the ant is on is black
            case '#': {
                board[antRow][antColumn] = ' ';
                switch (orientation) {
                    case NORTH: {
                        //checks for the edge of the board, turns around if it is at the edge
                        if ((antColumn - 1) < 0) {
                            orientation = EAST;
                            antColumn++;
                        } else {
                            orientation = WEST;
                            antColumn--;
                        }
                        //stores the ants location to change it in the next step
                        colorToChange = board[antRow][antColumn];
                        //places the ant
                        board[antRow][antColumn] = '*';

                        //displays board to user
                        for (int printRow = 0; printRow < boardRows; printRow++) {
                            int printColumn = 0;
                            cout << board[printRow][printColumn];

                            for (printColumn = 1; printColumn < boardColumns; printColumn++) {
                                cout << board[printRow][printColumn];
                            }
                            cout << "\n";
                        }
                    }
                        break;

                    case EAST: {
                        //checks for the edge of the board, turns around if it is at the edge
                        if ((antRow - 1) < 0) {
                            orientation = SOUTH;
                            antRow++;
                        } else {
                            orientation = NORTH;
                            antRow--;
                        }
                        //stores the ants location to change it in the next step
                        colorToChange = board[antRow][antColumn];
                        //places the ant
                        board[antRow][antColumn] = '*';

                        //displays board to user
                        for (int printRow = 0; printRow < boardRows; printRow++) {
                            int printColumn = 0;
                            cout << board[printRow][printColumn];

                            for (printColumn = 1; printColumn < boardColumns; printColumn++) {
                                cout << board[printRow][printColumn];
                            }
                            cout << "\n";
                        }
                    }
                        break;

                    case SOUTH: {
                        //checks for the edge of the board, turns around if it is at the edge
                        if ((antColumn + 1) >= boardColumns) {
                            orientation = WEST;
                            antColumn--;
                        } else {
                            orientation = EAST;
                            antColumn++;
                        }
                        //stores the ants location to change it in the next step
                        colorToChange = board[antRow][antColumn];
                        //places the ant
                        board[antRow][antColumn] = '*';

                        //displays board to user
                        for (int printRow = 0; printRow < boardRows; printRow++) {
                            int printColumn = 0;
                            cout << board[printRow][printColumn];

                            for (printColumn = 1; printColumn < boardColumns; printColumn++) {
                                cout << board[printRow][printColumn];
                            }
                            cout << "\n";
                        }
                    }
                        break;

                    case WEST: {
                        //checks for the edge of the board, turns around if it is at the edge
                        if ((antRow + 1) >= boardRows) {
                            orientation = NORTH;
                            antRow--;
                        } else {
                            orientation = SOUTH;
                            antRow++;
                        }
                        //stores the ants location to change it in the next step
                        colorToChange = board[antRow][antColumn];
                        //places the ant
                        board[antRow][antColumn] = '*';

                        //displays board to user
                        for (int printRow = 0; printRow < boardRows; printRow++) {
                            int printColumn = 0;
                            cout << board[printRow][printColumn];

                            for (printColumn = 1; printColumn < boardColumns; printColumn++) {
                                cout << board[printRow][printColumn];
                            }
                            cout << "\n";
                        }
                    }
                        break;
                }
            }

            default:
                break;
        }
    }
    return;
}
