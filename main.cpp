/*********************************************************************
 ** Program name: LangtonsAnt
 ** Author: David Anderson
 ** Date: 01/12/2019
 ** Description: This program runs the Langton's ant cellular automation
 *********************************************************************/

#include <iostream>
#include <limits>
#include "ant.hpp"

using std::cout;
using std::cin;

int boardColumns,
boardRows,
simulationSteps,
startRow,
startColumn,
choice;

std::string randomSelection,
            confirmBegin;

//this checks if the user has already played the simulation
bool playedYet = false;

bool goodInput = false;

bool randomAntPlacement = false;

//this menu function allows users to choose between playing the simulation and quitting
void menu()
{
    if (playedYet == false)
    {
        bool menuQuit = false;
        
        //this variable stores the users choice from the menu
        while (menuQuit == false)
        {
            while (goodInput != true)
            {
            //tells the user to pick an option
                cout << "What would you like to do?\n"
                << "1. Start Langton’s Ant simulation\n"
                << "2. Quit\n";
            
                //takes in the option that the user chooses
                cin >> choice;
                
                //correct answer
                if (choice == 1 || choice == 2)
                {
                    goodInput = true;
                    cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
                    break;
                }
                
                //incorrect answer
                if (choice != 1 || choice !=2)
                {
                    cin.clear();
                    cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
                    cout << std::endl;
                    goodInput = false;
                    cout << "Please enter a valid input\n";
                }

            }
            switch (choice)
            {
                case 1:
                {
                    cout << "Starting Langton's ant simulation...\n";
                    menuQuit = true;
                    break;
                }
                    
                case 2:
                {
                    cout << "Quitting...\n";
                    menuQuit = true;
                    return;
                    break;
                }
                    
                default:
                {
                    break;
                }
            }
        }
    }
    
    if (playedYet == true)
    {
        bool menuQuit = false;
        
        //this variable stores the users choice from the menu
        while (menuQuit == false)
        {
            goodInput = false;
            while (goodInput != true)
            {
                //tells the user to pick an option
                cout << "What would you like to do?\n"
                << "1. Play again\n"
                << "2. Quit\n";
            
                //takes in the option that the user chooses
                cin >> choice;
            
                //correct answer
                if (choice == 1 || choice == 2)
                {
                    goodInput = true;
                    cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
                    break;
                }
                
                //incorrect answer
                if (choice != 1 || choice !=2)
                {
                    cin.clear();
                    cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
                    cout << std::endl;
                    goodInput = false;
                    cout << "Please enter a valid input\n";
                }
            }
            switch (choice)
            {
                case 1:
                {
                    cout << "Starting Langton's ant simulation...\n";
                    menuQuit = true;
                    break;
                }
                    
                case 2:
                {
                    cout << "Quitting...\n";
                    menuQuit = true;
                    return;
                    break;
                }
                    
                default:
                {
                    break;
                }
            }
        }
    }
    return;
}

//this function gets the board rows/columns, steps, and ant placement
void setupBoard()
{
    //gets number of rows from user
    goodInput = false;
    while (goodInput != true )
    {
    cout << "How many rows should the board have? \n";
    cin >> boardRows;
        
        //bad answer
        if (boardRows < 2 || boardRows > 32766 || cin.fail())
        {
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout << std::endl;
            cout << "Please enter a valid input\n";
            goodInput = false;
        }
        
        //good answer
        if (boardRows >= 2 && boardRows <= 32766)
        {
            goodInput = true;
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            break;
        }
    }
    
    //gets number of columns from user
    goodInput = false;
    while (goodInput != true )
    {
        cout << "How many columns should the board have? \n";
        cin >> boardColumns;
        
        //bad answer
        if (boardColumns < 2 || boardColumns > 32766 || cin.fail())
        {
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout << std::endl;
            cout << "Please enter a valid input\n";
            goodInput = false;
        }
        
        //good answer
        if (boardColumns >= 2 && boardColumns <= 32766)
        {
            goodInput = true;
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            break;
        }
    }
    
    //gets number of steps from user
    goodInput = false;
    while (goodInput != true )
    {
        cout << "How many steps should the simulation have? \n";
        cin >> simulationSteps;
        
        //bad answer
        if (simulationSteps < 0 || simulationSteps > 32766 || cin.fail())
        {
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout << std::endl;
            cout << "Please enter a valid input\n";
            goodInput = false;
        }
        
        //good answer
        if (simulationSteps >= 0 && simulationSteps <= 32766)
        {
            goodInput = true;
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            break;
        }
    }
    
    //asks user if they want to randomly place the ant
    randomAntPlacement = false;
    goodInput = false;
    while (goodInput != true )
    {
        cout << "Should the ant placement be random? (y/n)\n";
        cin >> randomSelection;
        
        //good answers
        if (randomSelection == "y")
        {
            //generates random starting position
            startRow = rand() % boardRows + 1;
            startColumn = rand() % boardColumns + 1;
            
            randomAntPlacement = true;
            
            goodInput = true;
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            break;
        }
        
        if (randomSelection == "n")
        {
            randomAntPlacement = false;
            goodInput = true;
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            break;
        }
        
        //bad answer
        if (randomSelection != "y" || randomSelection != "n" || cin.fail())
        {
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout << std::endl;
            cout << "Please enter a valid input\n";
            goodInput = false;
        }
    }
    
    //gets ant starting row from user
    goodInput = false;
    while (goodInput != true && randomAntPlacement == false)
    {
        cout << "Which row should the ant start on? \n";
        cin >> startRow;
        
        //bad answer
        if (startRow < 1 || startRow > boardRows || cin.fail())
        {
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout << std::endl;
            cout << "Please enter a valid input\n";
            goodInput = false;
        }
        
        //good answer
        if (startRow >= 1 && startRow <= boardRows)
        {
            goodInput = true;
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            break;
        }
    }
    
    //gets ant starting column from user
    goodInput = false;
    while (goodInput != true && randomAntPlacement == false)
    {
        cout << "Which column should the ant start on? \n";
        cin >> startColumn;
        
        //bad snswer
        if (startColumn < 1 || startColumn > boardColumns || cin.fail())
        {
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout << std::endl;
            cout << "Please enter a valid input\n";
            goodInput = false;
        }
        
        //good answer
        if (startColumn >= 1 && startColumn <= boardColumns)
        {
            goodInput = true;
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            break;
        }
    }

    return;
    
}

int main()
{
    //shows menu to ask the user if they want to runs the simulation
    menu();
    
    while (choice != 2)
    {
        //gets board options from the user
        setupBoard();
    
        //passes information to the ant class
        ant ant1(boardRows, boardColumns, simulationSteps, startRow, startColumn);
    
        //runs the simulation
        ant1.runAnt();
        
        //sets a variable to trigger a different menu
        playedYet = true;
        
        //shows the menu again to ask the user if they want to play again
        menu();
    }
    
    return 0;
}
